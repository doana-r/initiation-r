
# Formation d’initiation à R

<!-- badges: start -->
[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
<!-- badges: end -->



L’index de cette formation se trouve à l’adresse :
[doana-r.gitlab.io/projets/initiation-r/](https://doana-r.gitlab.io/projets/initiation-r/)
<!-- A METTRE A JOUR -->



## Plan pour la suite


### Introduction à l’utilisation de R

### Introduction à la reproductibilité

### Exploration d’un jeu de données

### Introduction à la modélisation paramétrique


- Respecter les hypothèses d'un glm (transformation de variable) **A FAIRE**
- Expliquer la variabilité des données récoltées (modèle linéaire généralisé) **A FAIRE**
- Comparer les modalités d'une variable explicative **A FAIRE**
- Prédire (predict.lm et validation croisée) **A FAIRE facultatif**
- Prendre en compte la dépendance des individus (modèles mixtes) **A FAIRE facultatif**

### Introduction aux analyses multivariées

- Etudier les corrélations de nombreuses variables (analyses factorielles) **A FAIRE facultatif**
- Classer les individus (clustering) **A FAIRE facultatif**




## Déploiement : pense-bête personnel

### Présentations

Les présentations **xaringan** sont compilées en html (*infinite moon reader* et *knit*) le `.gitlab-ci.yml` permet de les mettre en ligne directement.

  - gitlab \> CI \> from template \> html
  - Facultatif : nommer un fichier `index.html` pour que l’adresse
    **doana-r.gitlab.io/nom\_du\_projet** soit desservie
  - Ne pas oublier de recompliler les `.Rmd` avant *commit*
  - Pour transformer tous les fichiers html en pdf avant de les transmettre, utiliser la fonction `transformer_en_pdf()`

### Tutoriels

Les tutoriels **learnr** sont mis en ligne via shiynapps.

  - Créer un dossier indépendant contenant le tuto et ses propres feuilles de style
  - Publier le document sur shinyapps (le `.html` est inutile pour le
    déploiement)
  - Mettre `rsconnect/` et `.html` dans `.gitignore` avant *commit*
