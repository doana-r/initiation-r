---
title: "Joindre des tables"
subtitle: ".doana-title[![](img/logoDoAna.png)]"
author: "DoAna - Statistiques Réunion"
institute: ""
date: "2024"
output:
  xaringan::moon_reader:
    lib_dir: libs
    chakra: libs/remark-latest.min.js
    df_print: kable
    css: ["css/presDoAna-fonts.css", "css/presDoAna.css", "css/classDoAna.css", "default"]
    nature:
      ratio: '16:9'
      highlightLines: true
      countIncrementalSlides: false
---
layout: true

.my-footer[![](img/logoDoAna.png) .footer-title[Combiner des tables]]



---
## Deux tables en relation
***

.pull-left[
![](img/join_setup.png)
]

--

.pull-right[
![](img/join_setup2.png)
]

???
notions : 
- base de données relationnelle

- clé primaire dans table x et clé étrangère dans table y forment une relation

- relation 1-n, 1-1 ou n-n


---
## Combiner tel quel
***

.pull-left[

```{r, eval=FALSE}
bind_rows(x, y)
```

![](img/join_bind-cols.png)
]

.pull-right[
```{r, eval=FALSE}
bind_cols(x, y)
```

![](img/join_bind-rows.png)
]


???
attention à l'ordre des colonnes / lignes !

---
## Combiner via une clé
***

```{r, eval=FALSE}
inner_join(x, y)
```

![](img/join_inner.png)

???
inner join

---
## Combiner via une clé
***

.pull-left[
```{r, eval=FALSE, tidy=FALSE}
left_join(x, y)






right_join(x, y)






full_join(x, y)

```
]
.pull-right[
.height450[![](img/join_outer.png)]
]


???
outer join

---
## Combiner via une clé
***

<br>


.tidy[![](img/join_venn.png)]



---
## Equivalence avec base R
***

<br>

<br>


**dplyr**          |	base R
-------------------|---------------------------------------------------
`inner_join(x, y)` |  `merge(x, y)`
`left_join(x, y)`  |	`merge(x, y, all.x = TRUE)`
`right_join(x, y)` |	`merge(x, y, all.y = TRUE)`
`full_join(x, y)`  |	`merge(x, y, all.x = TRUE, all.y = TRUE)`





---
## Quand la clé n'est pas unique
***


- une table (relation `1-n`)

![](img/join_one-to-many.png)

---
## Quand la clé n'est pas unique
***

- deux tables (relation `n-n`) /!\

![](img/join_many-to-many.png)

???
en général, c'est pas voulu...


---
## Définir la clé de jointure
***

<br> 

Nature   |  Détail  |      Code                       
---------|----------|-------------------------------
Par défaut | clé =  variables qui apparaissent dans les deux tables (**jointure naturelle**) |`by = NULL`
Vecteur texte | quand il faut préciser quelle est la clé car il y a ambiguité (sous-échantillon des variables en commun) | `by = c("col1", "col2")`
Vecteur texte nommé | quand la clé ne s'appelle pas de la même manière dans les deux tables | `by = c("col_x" = "col_y")`



---
## Utiliser une jointure pour filtrer
***



- `semi_join(x, y)` conserve les observations de `x` qui sont dans `y`


.pull-left[
![](img/join_semi.png)
]

.pull-right[
![](img/join_semi-many.png)
]


???
Les jointures précédentes servent à ajouter de nouvelles variables dans la table de référence


---
## Utiliser une jointure pour filtrer
***


- `anti_join(x, y)` supprime les observations de `x` qui sont dans `y` (aide pour diagnostiquer les problèmes !)

![](img/join_anti.png)


---
## Problèmes courants
***

- Identifier la clé (une ou plusieurs variables) qui est un **identifiant unique pertinent**
  
  
  ```{r, eval=FALSE}
  jeu |> count(clé) |> filter(n > 1)
  ```

--

- Vérifier que les valeurs des deux clés (étrangère et primaire) matchent bien 
  
  ```{r, eval=FALSE}
  anti_join(table1, table2) # table1 = table de référence
  ```
  
--

- Compter les lignes avant et après la jointure aide mais ne suffit pas



???
1. id non pertinent : latitude et longitude pour identifier les aéroports, alors qu'on connaît leur nom

2. En général : problèmes liés à la saisie des données, très pénibles à voir et à réparer...

3. Le cas si des valeurs répétées dans une clé pour une jointure interne


---
class: inverse, bottom, center
### Vous avez toutes les cartes pour gérer au mieux la logique en base de données relationnelle !

--

&nbsp;

--

&nbsp;

--

### ☺

--

&nbsp;

--

&nbsp;

--
[Accueil](/)
