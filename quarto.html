<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Aller vers une démarche documentée et reproductible avec quarto</title>
    <meta charset="utf-8" />
    <meta name="author" content="DoAna - Statistiques Réunion" />
    <meta name="date" content="2024-01-01" />
    <script src="libs/header-attrs/header-attrs.js"></script>
    <link href="libs/remark-css/default.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/presDoAna-fonts.css" type="text/css" />
    <link rel="stylesheet" href="css/presDoAna.css" type="text/css" />
    <link rel="stylesheet" href="css/classDoAna.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

.title[
# Aller vers une démarche documentée et reproductible avec quarto
]
.subtitle[
## .doana-title[<img src="img/logoDoAna.png" />]
]
.author[
### DoAna - Statistiques Réunion
]
.date[
### 2024
]

---

layout: true

.my-footer[![](img/logoDoAna.png) .footer-title[Aller vers une démarche documentée et reproductible]]

---

name: plan
## Sommaire
***

.pull-left[
1) Introduction

2) Ecrire

- Ecrire du code
- Ecrire du texte
- Les tables en sortie
- Du code dans le texte
]

.pull-right[
3) Compiler

- Compiler le rapport
- Les options globales
- Les options des *chunks*
- Débogage

4) Publier

- Différentes sorties possibles
- Mettre un rapport en ligne
- Plusieurs rapports groupés avec `params`
- La bibliographie
]


---

class: inverse, center, middle
# Introduction




---

background-image: url(img/quarto_script-html.png)

---
class: center, middle

# A quoi ça pourrait te servir ?

---

### Intérêt


- Code, résultats et prose combinés dans un seul fichier

- Reproductibilité assurée

- Sortie adaptable : PDF, page web, Word, présentation, document interactif (type shiny), site Internet

--

### Usages possibles


1. Communiquer aux décisionnaires : focus sur les résultats et non sur le code sous-jacent

2. Collaborer avec d'autres scientifiques : focus sur le chemin pour arriver aux conclusions

3. Conserver une trace de ses travaux et de ses pensées dans ce cahier de laboratoire moderne



---
## Trois éléments dans un .qmd
***

.pull-left[
Logiciel **quarto** installé automatiquement avec RStudio.

- l'en-tête (*YAML*) entourée de `---`

- le code R (*chunks*) entourées de ` ``` `

- du texte simple avec un formatage de type markdown


### Ressources indispensables

- quarto.org/docs/get-started
- Help &gt; Markdown Quick Reference
- la *cheat sheet* !
]

.pull-right[
![](img/quarto_script.png)
]


---
template: plan
---
class: inverse, center, middle
# Ecrire

---
## Ecrire du code
***


Créer un nouveau rapport : File &gt; New file &gt; Quarto document...

--

Créer un nouveau *chunk* :

- Raccourci clavier : Ctrl + Alt + I (recommandé)
- Bouton RStudio : .inlineimg[![](img/quarto_chunk.png)]
- Manuellement écrire (non recommandé) : `  ```{r}  `  et ` ``` `

&gt; Un chunk = une seule tâche (en général)

--

- Lancer le code d'un *chunk* : `Ctrl + Maj + Entrer` ou la flèche verte en haut à gauche du chunk
- Lancer une partie du *chunk* : `Ctrl + Entrer`
- Lancer tous les *chunk* : `Ctrl + Alt + R`

--

&gt; On ne se sert plus de la console comme lorsque l'on travaille avec des scripts.

---


## Ecrire du texte
***

Le plus simple pour démarrer est d'utiliser le mode visual !

La barre .inlineimg[![](img/quarto_barre.png)] permet de formater le texte très simplement !

--

### Exercice

Avec le **Markdown quick reference**, trouve comment :

- Ajouter une note de bas de page
- Ajouter une ligne horizontale
- Ajouter du code "inline"


---
## Les tables en sortie
***

.pull-left[
Par défaut :

``` r
cars[1:5, ]

# #   speed dist
# # 1     4    2
# # 2     4   10
# # 3     7    4
# # 4     7   22
# # 5     8   16
```
]

--

.pull-right[
Avec la fonction `kable()` :

``` r
knitr::kable(cars[1:5, ])
```



| speed| dist|
|-----:|----:|
|     4|    2|
|     4|   10|
|     7|    4|
|     7|   22|
|     8|   16|



&lt;!-- Possibilité de le faire systématiquement en indiquant dans le YAML (attention aux espaces) : --&gt;
&lt;!-- ``` --&gt;
&lt;!-- output: --&gt;
&lt;!--   html_document: --&gt;
&lt;!--     df_print: kable --&gt;
&lt;!-- ``` --&gt;


]

???
Aide supplémentaire :
`?knitr::kable`

Voir aussi les packages : xtable, stargazer, pander, tables et ascii


---
## Du code dans le texte
***

Le *inline code* s'écrit entre ``` ` ``` :



``` md
On a les données de `r nrow(diamonds)` diamants. Seuls `r nrow(diamonds) - nrow(smaller)` font plus que 2,5 carats.
```


Et cela donne en sortie :

&gt; On a les données de 53940 diamants. Seuls 126 font plus que 2,5 carats.

--

Attention aux nombres réels, la fonction `format()` est votre amie pour gérer le nombre de décimales et le séparateur de milliers:

``` r
format(x, digits = 2, big.mark = " ")
```

---
template: plan
---

class: inverse, center, middle
# Compiler


---
## Compiler le rapport
***

.pull-left[
Il suffit de cliquer sur .inlineimg[![](img/quarto_render.png)] ou de faire `Ctrl + Maj + K`.

La sortie dépend de ce qui est indiqué dans l'option `format` du YAML.


En ligne de commande :

``` r
library(quarto)
quarto_render("document.qmd")
```

### En coulisse

![](img/quarto_flow.png)

]

.pull-right[
![](img/quarto_html.png)
]



???
2 étapes de compilation :

- qmd -&gt; md via **knitr**, https://yihui.name/knitr/ : md inclue les sorties du code
- md -&gt; fichier final via **pandoc**, https://pandoc.org/ (hors de R)





---
## Les options globales dans le YAML
***

- YAML : *yet another markup language* représente des données hiérarchiques
- Attention aux indentations


``` md
---
title: "Mon document"
format:
  html:
    code-fold: true
    toc: true
---
```


&gt; Voir https://quarto.org/docs/reference/ pour avoir toutes les options possibles par format


---
## Les options des *chunks*
***

Gèrent si le code est executé et comment sont affichées les sorties. Quelques exemples :


Option |	Lance code |	Montre code |	Sortie |	Graphique |	Messages |	Warnings
-------|:-----------:|:------------:|:------:|:----------:|:--------:|:--------:
Par défaut (`true`)|	x |	x |x	 |x	 |x	 |	x 
`eval: false`| 	  |	     x     	|       |	         |	        |	
`include: false` |x	|	 |	 |	 |	 |	
`echo: false` 	|	x |	 |x	 |x	 |x	 |	x 				
`output: false` |x	|	x |	 |	 |x	 |	x		
`message: false` |x	|x	 |x	 |	x |	 | x		
`warning: false` |x	|x	 |x	 |	x |	x | 	

--

.pull-left[
Au niveau du chunk

```
#| echo: false
```

]

.pull-right[
Au niveau global


``` md
---
echo: false
---
```
]


---
## Débogage
***

Trouvez les erreurs est plus difficile car compiler avec **Render** nécessite qu'il n'y ait aucune erreur nulle part.  

Conseils :

- Lancer **Render** souvent

- Restart R `Ctrl + Maj + F10` puis Run all chunks `Ctrl + Alt + R` pour localiser l'erreur et travailler interactivement (avec la console)

- Parfois l'environnement de la console et celui de quarto sont différents (souvent problème de chemin relatif : lancer `getwd()` dans un chunk et préférer ranger les rapports en racine du projet !)

- Mettre `error: true` comme option dans le chunk qui pose problème


---
template: plan
---
class: inverse, center, middle
# Publier


---
## Différentes sorties possibles
***

[quarto.org/docs/guide/](https://quarto.org/docs/guide/)


![](img/quarto_sorties.png)


---
## Mettre un rapport en ligne
***

Conditions pour rendre public :

- volonté de partager un fichier à plus d’une personne
- pas d’infos confidentielles
- disponible sur du long terme

--

![](img/quarto_publier.png)


---
## Plusieurs rapports groupés avec `params`
***

Créer un paramètre pour filtrer les données par exemple :

![](img/quarto_params.png)




---
## La bibliographie
***

Dans le YAML : `bibliography: references.bib`

Formats pris en charge : bibTeX, endnote, medline.

Dans le texte pour citer : 


``` md
Séparer plusieurs citations par `;`: Bla bla [@smith04; @doe99].

Citation dans le texte : @smith04 dit bla, ou @smith04 [p. 33] dit bla.


A la fin (automatiquement)

# Bibliographie
```


Pour modifier le style de la bibliographie (styles courant [ici](http://github.com/citation-style-language/styles)) :


``` md
bibliography: references.bib
csl: apa.csl
```




---
class: inverse, bottom, center

### Vous allez aimer la reproductibilité !

--

&amp;nbsp;

--

&amp;nbsp;

--

### ☺

--

&amp;nbsp;

--

&amp;nbsp;

--

[Accueil](/)

    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="libs/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"ratio": "16:9",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// add `data-at-shortcutkeys` attribute to <body> to resolve conflicts with JAWS
// screen reader (see PR #262)
(function(d) {
  let res = {};
  d.querySelectorAll('.remark-help-content table tr').forEach(tr => {
    const t = tr.querySelector('td:nth-child(2)').innerText;
    tr.querySelectorAll('td:first-child .key').forEach(key => {
      const k = key.innerText;
      if (/^[a-z]$/.test(k)) res[k] = t;  // must be a single letter (key)
    });
  });
  d.body.setAttribute('data-at-shortcutkeys', JSON.stringify(res));
})(document);
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
