---
title: "Rapport voitures"
format: html
params:
  my_class: "suv"
---

```{r}
#| label: import
#| message: false

library(ggplot2) # graphiques
library(dplyr) # manipulation des données

mpg_class <- mpg |>  
  filter(class == params$my_class)
```

## Economie de fuel pour la classe `r params$my_class`

```{r}
#| echo: false
#| message: false
ggplot(mpg_class) +
  aes(x = displ, y = hwy) +
  geom_point() +
  geom_smooth()
```



