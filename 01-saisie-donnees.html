<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Saisie des données sous tableur</title>
    <meta charset="utf-8" />
    <meta name="author" content="DoAna - Statistiques Réunion" />
    <meta name="date" content="2024-01-01" />
    <script src="libs/header-attrs/header-attrs.js"></script>
    <link href="libs/remark-css/default.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/presDoAna-fonts.css" type="text/css" />
    <link rel="stylesheet" href="css/presDoAna.css" type="text/css" />
    <link rel="stylesheet" href="css/classDoAna.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

.title[
# Saisie des données sous tableur
]
.subtitle[
## .doana-title[<img src="img/logoDoAna.png" />]
]
.author[
### DoAna - Statistiques Réunion
]
.date[
### 2024
]

---

layout: true

.my-footer[![](img/logoDoAna.png) .footer-title[Saisie des données sous tableur]]

---
background-image: url(img/01-saisie_microtubules.png)
background-size: contain

???
Si on donne ça à manger à R, il ne sera pas content et le fera bien comprendre !

---


## Objectif

.pull-left[
### Provenance des données
- Instruments de mesure
- Bases de données en ligne
- Un cahier de laboratoire ou de terrain (données écrites manuellement)
- ...
]

--

.pull-right[
### Sauvegarde des données

- Dans une feuille de calcul
- Dans une base de données


### De telle sorte qu'elles soient

- Lisibles par une machine : un algorithme peut les lire
- Lisibles par les êtres humains : elles sont documentées
]



---
name: plan
## Sommaire
***

.pull-left[
1) Mise en forme des données

- Démarrage
- *Tidy data*

2) Les variables

- Choix des variables
- Remplissage des cellules
- Les dates
- Bien nommer
]
.pull-right[
3) Logique en base de données

- Echelles du jeu de données
- Documenter son jeu de données

4) Préparer les données en vue d'une importation sous R

- Tableaux croisés dynamiques et autres graphiques
- Formats de sauvegarde
]



---
class: inverse, center, middle

# Mise en forme des données






---

## Démarrage
***


- Feuille de saisie `\(\neq\)` Feuille de terrain
--

- Tableau rectangulaire **et c'est tout** !
--

- Saisir les données **brutes**

--

&gt; Calculs interdits dans le tableur

--

### *Tidy data*

- Individu ou observation `\(\leftrightarrow\)` Ligne
- Variable `\(\leftrightarrow\)` Colonne
- Valeur `\(\leftrightarrow\)` Cellule


---
name: tidy

## *Tidy Data*
***



Plusieurs manières de stocker des données :

.tidy[
{{content}}
]

???
C'est un travail en amont, qui fait gagner du temps !

Source : r4ds.had.co.nz


---
template: tidy

![](img/01-saisie_tidy-1.png)

---
template: tidy

![](img/01-saisie_tidy-2.png)

---
template: tidy

![](img/01-saisie_tidy-3.png)

---
template: tidy

![](img/01-saisie_tidy-4.png)

---
template: tidy

- Approche inférentielle `\(\leftrightarrow\)` Format long

- Approche multivariée `\(\leftrightarrow\)` Format court

--

&gt; Les données tidy sont les plus simples à importer dans R.

???
Il est possible d'avoir des données non-tidy assumées.

---
## Exercice
***

Combien pouvez-vous trouver de variables ?
![](img/01-saisie_ex-untidy.png)
---
## Solution
***
![](img/01-saisie_ex-tidy.png)

???
Difficile de faire cela avec R (mais ça se fait), contrairement aux exemples précédents.

D'où l'intérêt de rentrer correctement les données dès le départ.

Source : https://peerj.com/preprints/3183/

---
template: plan


---
class: inverse, center, middle
# Les variables

---
name: variables
## Choix des variables
***


- Une colonne = une variable = une information
- Une seule ligne pour le nom des variables

{{content}}


---
template: variables

### Variables qualitatives

- Ordinale ou nominale ?
- Une modalité = ce qui est observable
- Généralement, nombre limité de modalités à prévoir à l'avance si possible

???
- La couleur des yeux : bleu, vert, marron
- A quelle fréquence regarde-t-on la télévision : jamais, rarement, souvent

---
template: variables


### Variables quantitatives

- Continue ou discrète ?
- Ce que l'on a mesuré ou compté, **pas de calculs**
- Pas d'unités de mesure
- Etre conscient du séparateur décimal : `","` ou `"."`
- Si UN caractère texte présent dans la colonne,
  celle-ci sera interprété entièrement comme du texte !

???

- mesure de la hauteur d'arbres
- comptage d'insectes

---

## Remplissage des cellules
***

### Cas général
- Remplir **toutes** les cellules
- Ne pas fusionner les cellules
- Les couleurs ne seront pas importées
- Attention à l'orthographe des modalités

--

### Valeurs manquantes

- Utiliser le même codage
- Convention `"NA"` (*not available*)
- Choix entre `"0"` (il n'y en a pas) et `"NA"` (cela n'a pas pu être observé) parfois difficile

???
Une parcelle qui crame : rendement de 0 (rien de récolté) ou NA (la parcelle sort de l'expérimentation) ?


---

## Les dates
***

- Indiquer l'année
- Formats recommandés : `jj/mm/yyyy` ou `yyyy-mm-jj` ou même `yyyymmjj`
- Se tenir à un seul format
- **Attention** : valeur stockée `\(\neq\)` valeur affichée
--

- Stockage en nombre de jours depuis une date référence (R : `1970-01-01`)

???

- Si pas d'année et saisie faite l'année d'après : décalage

- On peut aussi faire 3 colonnes, pour plus de sécurité : année mois jour

- Préférence personnelle pour le format `yyyy-mm-jj` car ordre chronologique = ordre alphanumérique et `yyyymmjj` car pas de risque d'interprétation d'Excel



---
name: nommer
## Bien nommer pour satisfaire R
***

Choisir des noms simples, courts, informatifs.

{{content}}


---
template: nommer

### Nom des variables

- Homogénéiser
- Casse
- Caractères spéciaux interdits
- Espaces interdits
- Complétion

---
template: nommer

### Nom des modalités

- Faire des abbréviations, mais pas trop
- Casse
- Caractères spéciaux possibles mais attention (`#`)
- Espaces possibles
  Attention à `"tomate"` et `"tomate "`
- Complétion parfois


---
template: nommer

### Convention

**snake_case** recommandé :

- `nom de variable` devient `nom_de_variable`
- `NomDeVariableUpperCamelCase` devient `nom_de_variable_upper_camel_case` (ce qui pourrait être raccourci non ?)
- `Variable` devient `variable`

--

&gt; Pour les noms de variables et les noms d'objets dans R

---
template: nommer

### Exemple de codage utile

Pour les noms d'espèces :

- L'arabette des dames ou *Arabidopsis thaliana* devient `ARATHA`
- La truite arc-en-ciel ou *Oncorhynchus mykiss* devient `ONCMYK`

---
template: plan

---
class: inverse, middle, center

# Logique en base de données relationnelle


---
background-image: url(img/01-saisie_BDD.png)

## Exemple : relevés d'espèces
***

&amp;nbsp;

&amp;nbsp;

&amp;nbsp;

&amp;nbsp;

&amp;nbsp;

&amp;nbsp;

&amp;nbsp;

&amp;nbsp;

&amp;nbsp;

**Jouons à** : qu'est-ce qui va bien et qu'est-ce qui ne va pas ?

???

Bien :
- Tableaux rectangulaires
- Logique en BDD : écononie de saisie + organisation
- Le format des dates


Pas bien :
- La clé
- les noms des variables
- les cases vides
- La modalité herbicide qui devrait être "oui"


Source : données réelles, mais on ne va pas dénoncer


---

## Echelles du jeu de données
***

- Penser en terme d'individus statistiques
- Séparer en plusieurs tables avec peu de colonnes
- Une table par feuille
- Choix des clés pour faire les liens entre les tables

--

Avantage principal : économie de saisie `\(\rightarrow\)` moins d'erreurs

--

&gt; Organisation des données à penser en amont.

---

## Documenter son jeu de données
***

&gt; Le tableur doit se suffire en lui-même

--

Créer un onglet "métadonnées" dans le tableur qui donne :

- **à quoi sert** chaque table

- pour chaque table, quelle combinaison de colonnes 
  **définit ses lignes** de manière unique
  
- que représente **chaque variable**
  (description en français, unité, modalités possibles, 
  format du code, lien vers plus d'info, etc.)



---

## Documenter son jeu de données
***

&gt; Le tableur doit se suffire en lui-même



![](img/01-saisie_BDD-info.png)

---
template: plan

---
class: inverse, middle, center

# Préparer les données en vue d'une importation sous R

---

## Tableaux croisés dynamiques et autres graphiques
***


- Visualisation rapide et vérification de la saisie
- A faire dans une nouvelle feuille
- Mise à jour automatique quand les données sont modifiées

&amp;nbsp;


.center[![](img/01-saisie_TCD.png)]


---

## Formats de sauvegarde
***



Caractéristique|Tableur|Texte
---|---|---
Type de fichier|binaire|brut
Extention|.xlsx ou .ods|.txt, .tsv, .csv
Suivi des modifications| **-** | **+**
Taille et utilisation de la mémoire| **-** | **+**
Import dans R | **+** | **-**
Contient toutes les infos nécessaires| **+** | **-**
Format de la saisie contrôlé | **-** | **+**

--

&lt;br&gt;

&gt; File &gt; Save as...

---

## Formats de sauvegarde

***

&lt;br&gt;

Quels séparateurs de champs (= colonnes) possibles pour un fichier texte ?

&lt;br&gt;

Séparateur|Symbole|Format
---|---|---
Tabulation|`\t`|**.txt** ou **.tsv**
Virgule|`,`|**.csv** anglais
Point-virgule|`;`|**.csv** français



???

.csv enregistre ce qui est affiché (dates)

---
class: inverse, bottom, center

### Maintenant vous savez comment avoir des données toutes propres

--

&amp;nbsp;

--

&amp;nbsp;

--

### ☺

--

&amp;nbsp;

--

&amp;nbsp;

--

[Accueil](/)



    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="libs/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"ratio": "16:9",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// add `data-at-shortcutkeys` attribute to <body> to resolve conflicts with JAWS
// screen reader (see PR #262)
(function(d) {
  let res = {};
  d.querySelectorAll('.remark-help-content table tr').forEach(tr => {
    const t = tr.querySelector('td:nth-child(2)').innerText;
    tr.querySelectorAll('td:first-child .key').forEach(key => {
      const k = key.innerText;
      if (/^[a-z]$/.test(k)) res[k] = t;  // must be a single letter (key)
    });
  });
  d.body.setAttribute('data-at-shortcutkeys', JSON.stringify(res));
})(document);
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
