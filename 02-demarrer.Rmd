---
title: "Démarrer sous RStudio"
subtitle: ".doana-title[![](img/logoDoAna.png)]"
author: "DoAna - Statistiques Réunion"
institute: ""
date: "2024"
output:
  xaringan::moon_reader:
    lib_dir: libs
    chakra: libs/remark-latest.min.js
    df_print: kable
    css: ["css/presDoAna-fonts.css", "css/presDoAna.css", "css/classDoAna.css", "default"]
    nature:
      ratio: '16:9'
      highlightLines: true
      countIncrementalSlides: false
---
layout: true

.my-footer[![](img/logoDoAna.png) .footer-title[Démarrer sous RStudio]]

---
name: plan
## Sommaire
***

1) R

- Qu'est-ce que R ?
- Historique
- Les objets
- Quelques grands principes
- Avantages de R
    
2) RStudio

- Qu'est-ce que RStudio ?
- Apparté sur les sauvegardes
- Avantages de RStudio

3) Les packages
    
- Organisation des fonctions existantes
- Stockage
- Mise à jour

---

class: inverse, center, middle

# R
## la programmation pour les jeux de données


```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)

knitr::opts_chunk$set(
  collapse = TRUE,
  fig.show = 'hold',
  fig.asp = 0.618,
  fig.width = 7,
  out.width = "70%",
  fig.align = "center"
  )
```

---

## Qu'est-ce que .inlineimg[![](img/logoR.png)] ?
***

--

### Langage

Langage de programmation basé sur le langage **S**

- Langage interprété (utilisation de scripts)

- Programmation **procédurale** (utilisation de fonctions) et, avec certaines fonctions, programmation **orientée-objet**

--

### Logiciel

Logiciel qui interprète les commandes rédigées en R

- Interface graphique → console (terminal de commandes)

--

> Ouvrez R seul !

???
Sert à manipuler, visualiser, analyser des jeux de données

Exercice : 

- lancer quelques commandes
- découvrir le `>` et le `+`

---
## Historique
***

- 1993 : projet de recherche (**R**oss Ihaka et **R**obert Gentleman) inspiré du langage S, au département Statistiques de l'université d'Auckland (Nouvelle-Zélande)

- 1995 : publication de R sous **license GNU** : code open-source

- 1997 : centralisation de toutes les informations sur R via le CRAN (*comprehensive R Archive network*) sur [R-project.org](https://cran.r-project.org/)

- 2000 : première version stable : `R 1.0` !  

- 2007 : invention de **ggplot2** par Hadley Wickham

- 2016 : création du **tidyverse**



---
## Les objets
***

> **Tout** est objet dans R !

--


Objet|Nom|Détail
---|---|---
Vecteur atomique|`vector`|Objet élémentaire
Matrice|`matrix`|Extention des vecteurs à 2 dimensions
Liste|`list`|Sac fourre-tout
Jeu de données|`data.frame` ou `tibble`|Cas particulier de liste
Fonction|`function`|Objet d'action



???
vecteur : abus de langage car comprends techniquement les listes


---
name: principes
## Quelques grands principes
***

{{content}}

---
template: principes

### Utilisation des fonctions

- Pour qu'il se passe quelque chose dans R

--

![](img/02-demarrer_putput.jpg)


???
pas d'input : `q() ` 

pas de sortie (*side effect*) : les fonctions graphiques ou d'export d'un fichier à l'extérieur de R

---
template: principes

### Utilisation des fonctions

- Trois manières de les utiliser :

```{r fct1}
factorial
```
--
```{r fct2}
factorial(4)
```
--
```{r fct3, eval=FALSE}
?factorial
```



---
template: principes


### Affectation
Pointer la sortie d'une commande vers un nom (un objet nouveau ou existant), afin de pouvoir la réutiliser facilement : `est_ce <- "utile ?"`.

--

**Exercice :**

Choisissez ce qui est syntaxiquement correct :

1. `ananas <- 5`
1. `ananas = 5`
1. `5 -> ananas`
1. <code>\`5\` <- "ananas"</code>
1. `5 <- "ananas"`
1. `ananas <- "5"`


???
Réponse : tout sauf `5 <- "ananas"`

Préférer : `ananas <- 5` et `ananas <- "5"`


---
template: principes

### Indexation
Extraire une partie des éléments d'un objet par leurs positions, leurs noms ou grâce à une question.

--

Exemples écrits en français :

- Position : je veux le premier et le dernier élément
- Noms : dans un tableau, je veux la colonne `hauteur` et la colonne `identifiant`
- Question : je veux sélectionner tous les individus qui ont les yeux marrons et qui ont moins de 30 ans

???
On s'entrainera à écrire cela en R plus tard

---
template: principes

### Vectorisation
Privilégier le calcul *"vectoriel"* au calcul itératif (boucles) car gain en temps de calcul et allègement de l'écriture.

```{r vectorisation, eval=FALSE}
   _      __________      _
  | |    |          |    | |
  |v| -> | function | -> |v|
  |_|    |__________|    |_|

```

--

**Exercice :**

Choisissez ce qui est correct. Dans R, un vecteur :

1. contient des éléments de nature différente
1. peut avoir une longueur de `0`
1. peut avoir une longueur de `1`
1. son premier élément est indexé par `0`



???
Réponses :
1. non, les éléments d'un vecteur sont tous de même nature
1. oui
1. oui
1. non, contrairement à d'autres langages, le premier élément est indexé par `1`



---
## Avantages de R
***

- Logiciel de pointe en statistiques en évolution constante

- Reproductibilité : le **chemin** est plus important que le résultat (contrairement à Excel par exemple)

- Automatisation des tâches

- Accès au code source

- Gratuité

- Communauté gigantesque (utilisateurs et développeurs) et réactive

--

**MAIS** besoin d'un environnement de développement (interface graphique) !

---
template: plan

---

class: inverse, center, middle

# RStudio
## un environnement de développement dédié à R


---
## Qu'est-ce que .inlineimg[![](img/logoRstudio.png)] ?
***


- **Environnement de développement** (*integrated development environment*) : augmentation de la productivité des programmeurs en automatisant une partie des activités et en simplifiant les opérations.

- Développement par `Posit` (anciennement `RStudio, Inc.`), une entreprise commerciale fondée par Joseph J. Allaire. 

- Première version : février 2011, en développement actif

- Gratuit, open-source, multiplateforme

- Autres interfaces possibles : Emacs (éditeur de texte), tinnR (Windows), Jupyter (python), ...



???
Exercice : ouvrir RStudio


---
background-image: url(img/02-demarrer_recette-rstudio.jpg)
background-size: contain
class: inverse



---
## Apparté sur les sauvegardes
***

> Ménage automatique : à la fin de la session → comptoir vidé

--


Type|Cuisine|Contenu|Extention
---|---|---|---
**Source**|Recette|Lignes de code|`.R`
**Environnement**|Comptoir|Objets R|`.RData` ou `.Rds`


--

Sauvegarde du **script** en priorité

--

Les `.RData` pour :

- charger plus rapidement des objets très lourds
- transporter des objets intermédiaires entre deux scripts


???
- Les ustentiles retournent dans leurs placards
Les plats et l'historique vont à la poubelle.
Ce qui nous intéresse : pas les plats, mais la recette !


---
## Les options à modifier
***

> Tools -> global options

--

Où | Quoi
---|---
General > Basic|Décocher `restore .RData at startup` <br> `save workspace to .RData on exit` -> Choisir `never` <br> Décocher `Always save history`
Code > Editing|Cocher `soft-wrap R-source files`
Code > Display|Cocher `Use rainbow parenthesis`
Apparance|`editor theme` -> Choisir `Cobalt`
Packages > Management|Choisir un dépot CRAN principal
Python|Décocher `Automatically activate Python environements`



---
## Avantages de RStudio
***

.pull-left[
- coloration syntaxique, complétion de code, intendation intelligente

- excécution du code R directement depuis l'éditeur (source)

- accès facilité à la documentation des fonctions

- gestion de plusieurs répertoires de travail à l'aide de projets

- utilisation d'extentions interactives (*addins*)
]
--
.pull-right[
- contrôle de version (git)

- débogueur interactif pour diagnostiquer les erreurs

- tutoriel interactifs

- outils de développement de packages

- outils d'optimisation du temps de calcul

- connexion à des bases de données externes

- ...
]

???
On en découvre tous les jours !

---
template: plan

---

class: inverse, center, middle
# Les packages
## unités de développement

---

## Organisation des fonctions existantes
***

Unité de regroupement des fonctions : le ***package***.

Synonymes : plug-in, module d'extention, ...

--

- R base : `r R.version.string` est composé à l'installation de `r nrow(installed.packages("/usr/lib/R/library"))` packages 

- Pour accéder à de nouvelles fonctionnalités, nouveaux packages disponibles sur le CRAN  
Aujourd'hui (`r Sys.Date()`) : 
`r nrow(available.packages(contriburl = contrib.url(repos = "https://pbil.univ-lyon1.fr/CRAN/", type = "source"),type = "source"))` packages disponibles sur le CRAN

--

### Où ?

```{r paths}
.libPaths()
```

```{r paths2, eval=FALSE}
library()
```


???
nouvelles fonctionnalités = nouvel ustensile

---
## Stockage
***


### Sous quel forme ?
![](img/02-demarrer_install-packages.png)

--

> Besoin de Rtools pour compiler les packages "source"



???
Exercice : regarder les libpaths de chacun


Source : r-pkgs.org (figure coupée)

---
## Mise à jour
***

Pourquoi mettre à jour ?

--

**R base (tous les 6 mois)**  


Etapes recommandées :

- installer la nouvelle version (sur [r-project.org](https://cran.r-project.org/))
- pour les mises à jour majeures (3.6.2 → 4.0.1) et mineures (4.0.1 → 4.1.0), déplacer les autres packages dans la bonne *library* 
- lancer `update.packages(checkBuilt=TRUE, ask=FALSE)` avec le nouveau R
- désinstaller l'ancienne version de R (ça fait plus propre)

--

**Les autres packages (dès qu'on utilise R)**  

 
Bouton inclu dans RStudio : .inlineimg[![](img/02-demarrer_update-packages.png)]

--

**RStudio (tous les mois)**  


Allez sur [rstudio.com](https://rstudio.com/products/rstudio/download/#download)

???


Débuggage, nouvelles fonctionnalités, attention aux *breaking changes*


Le modèle de casserole à maintenant une poignée plus ergonomique, on en rachète une nouvelle au magasin et on jète l'ancienne.



---
class: inverse, bottom, center

### Maintenant vous connaissez la différence entre **R** et **RStudio** et entre *package* et *library*

--

&nbsp;

--

&nbsp;

--

### ☺

--

&nbsp;

--

&nbsp;

--

[Accueil](/)


